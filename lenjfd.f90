!!!!This program implement lennard jones potential with finite difference method
!!!author : Hossein Tahmasbi 11 october 2015 , IASBS, Zanjan
program lenjfd
implicit none

integer::nat,iat,m,i,l
real(8)::epot,fatx,faty,fatz,rnd(3),h,fd,rb,c(-2:2), ampl
real(8),allocatable::fat(:,:), fatref(:,:)
real(8),allocatable::rat(:,:)

open(unit=1,file ='lj.dat',status='old')
read(1,*) nat
allocate(fatref(3,nat),fat(3,nat),rat(3,nat))

!write(*,*) 'Coordinates of atoms:'
!write(*,*) '********************************************************************'
do iat=1,nat
read(1,*)  rat(1,iat),rat(2,iat),rat(3,iat)
!write(*,*) rat(1,iat),rat(2,iat),rat(3,iat)
enddo

ampl=0.1d0
do iat = 1,nat
call random_number(rnd)
rat(1:3,iat) = rat(1:3,iat) + (rnd(1:3) - 0.5d0)*2.d0*ampl
enddo



!write(*,*) 'Coordinates of random atoms:'
!write(*,*) '********************************************************************'
!do iat=1,nat
!write(*,*) rat(1,iat),rat(2,iat),rat(3,iat)
!enddo

! apply finite difference method on potential energy
m=2
c(-2:2) = (/1.d0/12.d0,-2.d0/3.d0,0.d0,2.d0/3.d0,-1.d0/12.d0/)
h = 1.d-3

call lenjc(nat,rat,epot,fatref)

do iat = 1,nat

  do i = 1,3
    fd=0.d0
    rb=rat(i,iat)
         do l = -m ,m
           if(l==0) cycle
           rat(i,iat)=rb+l*h
           call lenjc(nat,rat,epot,fat)
           fd = fd+c(l)*epot
         enddo
    rat(i,iat)=rb
    fd=-fd/h !this is negative of gradient
  write(*,'(2i4,2es14.5)') iat,i,fatref(i,iat),(fd-fatref(i,iat))/fatref(i,iat)
  enddo

enddo


!write(*,*) '*********************************************************************'
!write(*,*) 'Potential Energy:'
!
!write(*,*) epot
!
!write(*,*) '*********************************************************************'
!write(*,*) 'Force on each atom:'
!
!
!fatx = 0.d0
!faty = 0.d0
!fatz = 0.d0
!
!do iat = 1,nat
!!write(*,*)  iat
!write(*,'(3es24.15)')  fatref(1,iat),fatref(2,iat),fatref(3,iat)
!fatx = fatx + fatref(1,iat)
!faty = faty + fatref(2,iat)
!fatz = fatz + fatref(3,iat)
!enddo
!
!write(*,*) '*********************************************************************'
!write(*,*) 'total force :'
!write(*,'(3es24.15)') fatx,faty,fatz


end program lenjfd


!*********************************************************************************

subroutine lenjc(nat,rat,epot,fat)

implicit none
integer::iat,jat
integer,intent(in)::nat
real(8),intent(in)::rat(3,nat)
real(8),intent(out)::epot,fat(3,nat)
real(8)::dx,dy,dz,r2,r2inv,r6inv,r12inv,xiat,yiat,ziat,vat
real(8)::tt,t1,t2,t3

epot = 0.d0
fat(1:3,1:nat) = 0.d0

do iat =1,nat
     xiat = rat(1,iat)
     yiat = rat(2,iat)
     ziat = rat(3,iat)
  do jat =iat+1,nat 


    dx=xiat-rat(1,jat)
    dy=yiat-rat(2,jat)
    dz=ziat-rat(3,jat)

    r2=dx**2+dy**2+dz**2
    r2inv = 1.d0/r2
    r6inv=r2inv**3
    r12inv=r6inv**2
    
    vat=4.d0*(r12inv - r6inv)

    epot =  epot + vat

    tt =24.d0*r2inv*(2.d0*r12inv-r6inv)
    t1=tt*dx;t2=tt*dy;t3=tt*dz

    fat(1,iat) = fat(1,iat) +  t1
    fat(2,iat) = fat(2,iat) +  t2
    fat(3,iat) = fat(3,iat) +  t3
    
    fat(1,jat) = fat(1,jat) -  t1
    fat(2,jat) = fat(2,jat) -  t2
    fat(3,jat) = fat(3,jat) -  t3

  enddo
enddo

end subroutine lenjc
