!*****************************************************************************************
!energy and forces for Lennard Jones potential
subroutine lenjon_reza(nat,rat,epot,fat)
    implicit none
    integer, intent(in):: nat
    real(8), intent(in):: rat(3,nat)
    real(8), intent(out):: epot, fat(3,nat)

    !local variabbles
    integer:: iat, jat
    real(8):: xiat, yiat, ziat, dx, dy, dz, rsq, rinvsq, rinv4, rinv6, rinv12
    real(8):: tt, t1, t2, t3
    epot=0.d0
    fat(1:3,1:nat)=0.d0
    do iat=1,nat
        xiat=rat(1,iat)
        yiat=rat(2,iat)
        ziat=rat(3,iat)
        do jat=iat+1,nat
            dx=rat(1,jat)-xiat
            dy=rat(2,jat)-yiat
            dz=rat(3,jat)-ziat
            rsq=dx**2+dy**2+dz**2
            rinvsq=1.d0/rsq
            rinv4=rinvsq**2;rinv6=rinvsq*rinv4;rinv12=rinv6**2
            epot=epot+4.d0*(rinv12-rinv6)
            tt=24.d0*rinvsq*(2.d0*rinv12-rinv6)
            t1=dx*tt;t2=dy*tt;t3=dz*tt
            fat(1,iat)=fat(1,iat)-t1  
            fat(2,iat)=fat(2,iat)-t2  
            fat(3,iat)=fat(3,iat)-t3  
            fat(1,jat)=fat(1,jat)+t1
            fat(2,jat)=fat(2,jat)+t2
            fat(3,jat)=fat(3,jat)+t3
        enddo
    enddo
end subroutine lenjon_reza
!*****************************************************************************************
