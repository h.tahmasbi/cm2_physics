!Part3_subroutine for Helium: This program implement the schrodinger equation for the hydrogen atom and calculate the hartree and exchange energy with
!basis sets that are obtained by PC SD(program 1hydrogenpc.f90) 
!author: Hossein Tahmasbi,Tuesday 8 Deceber 2015, IASBS, Zanjan
subroutine harex_ener(znuc,nrad,rrad,urad,rho,vH,Eh,Ex,VCA)
   implicit none
   integer::i,nrad,lang,iter,j,irad,jrad
   real(8),allocatable::rrad(:),urad(:),hurad(:),surad(:),g(:),sgrad(:),hhp(:,:),ssp(:,:)
   real(8)::znuc,ener,gnrm,uradnrm,sumn,gama,pi
   real(8),allocatable::p(:),w0(:),rho(:),rhot(:),w(:),vH(:),v1(:),v2(:),v1n(:),v2n(:),EC(:),VCA(:),VCB(:)
   real(8)::rhotn,Eh,Ex,eta
   complex(8),allocatable::gc(:)
 nrad = 10000
 allocate(urad(0:nrad),rrad(0:nrad),hurad(0:nrad),surad(0:nrad),g(0:nrad),sgrad(0:nrad),hhp(2,0:nrad),ssp(2,0:nrad),VCB(0:nrad))
 allocate(p(0:nrad),gc(0:nrad),w0(0:nrad),rho(0:nrad),rhot(0:nrad),w(0:nrad),vH(0:nrad),v1(0:nrad),v2(0:nrad),v1n(0:nrad),v2n(0:nrad),EC(0:nrad),VCA(0:nrad))
 pi = 4.d0*atan(1.d0)
 eta = 1.d0
 lang = 0
!znuc = 1.d0
 gama = 1.d-1
! call radgrid(nrad,rrad)
! do i = 0,nrad
! urad(i) = exp(-rrad(i)**2)
! enddo
!  
!do iter = 0,10
!
! call overlap(nrad,rrad,urad,sumn,surad)
! uradnrm = sqrt(sumn)
! do i = 0,nrad
! urad(i) = urad(i)/uradnrm
! enddo
! 
! call overlap(nrad,rrad,urad,sumn,surad)
! call energr(nrad,lang,znuc,rrad,urad,ener,hurad)
!
! g = hurad - ener*surad
! call overlap(nrad,rrad,g,sumn,surad)
! gnrm = sqrt(sumn)
! write(*,'(i6,es24.15,es14.5)') iter,ener,gnrm
! if (gnrm < 1.d-3) exit
!
! call crtssp(nrad,rrad,ssp)
! call crthhp(nrad,lang,znuc,rrad,hhp)
! gc = cmplx(g,0.d0,8)
! call ctridag(nrad+1,hhp,ssp,ener,gama,gc)
! p = real(gc,8)
!
! do i=0,nrad
! urad(i) = urad(i) - p(i)
! enddo
!
!enddo
!****************************************************************end of preconditioned SD   
  do i = 0,nrad
    if (i==0) then
    w0(i) = (rrad(1)-rrad(0))/2
    elseif (i==nrad) then
        w0(i) = (rrad(nrad) - rrad(nrad-1))/2
    else
        w0(i) = (rrad(i+1) - rrad(i-1))/2
    endif
  enddo

  do i = 0,nrad
  rho(i) =  1/(4*pi)*urad(i)**2  
  enddo
  
  do i = 0,nrad
  rhot(i) = 4*pi*rho(i)*rrad(i)**2*w0(i)
  enddo
  rhotn = sum(rhot(0:nrad))


 do i = 0,nrad


  v1(i) =  4*pi*rho(i)*rrad(i)**2*w0(i)
  v1n(i) = sum(v1(0:i))

  v2(i) =  4*pi*rho(i)*rrad(i)*w0(i)
  v2n(i) = sum(v2(0:i))

 enddo
  
 do i = 1,nrad
  vH(i) = v1n(i)/rrad(i) + (v2n(nrad)-v2n(i))
 enddo

 do i = 0,nrad
 if (rho(i) > 1.d-20) then
     call LSD_PADE(rho(i),eta,EC(i),VCA(i),VCB(i))
 else
     write(*,*) 'all output = 0.d0'
 endif
 enddo
Eh =0.d0
Ex =0.d0

  do i = 0,nrad
  Eh = Eh + 4*pi*1/2*vH(i)*rho(i)*w0(i)*rrad(i)**2
  Ex = Ex + 4.d0*pi*EC(i)*rho(i)*rrad(i)**2*w0(i)
!or Ex = Ex + EC(i)*urad(i)**2*rrad(i)**2*w0(i)
  enddo 
!write(*,'(2es24.15,es14.5)') rhotn,Eh,Ex

end subroutine harex_ener
