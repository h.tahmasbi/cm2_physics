!Part2_1: This program implement the schrodinger equation for the hydrogen atom and calculate the hartree and exchange energy with
!basis sets that are obtained by PC SD(program 1hydrogenpc.f90) 
!author: Hossein Tahmasbi,Tuesday 8 Deceber 2015, IASBS, Zanjan
program hydrogenhex
   implicit none
   integer::i,nrad,lang,iter,j,irad,jrad
   real(8),allocatable::rrad(:),urad(:),hurad(:),surad(:),g(:),sgrad(:),hhp(:,:),ssp(:,:)
   real(8)::znuc,ener,gnrm,uradnrm,sumn,gama,pi
   real(8),allocatable::p(:),w0(:),rho(:),rhot(:),w(:),vH(:),v1(:),v2(:),v1n(:),v2n(:),EC(:)
   real(8)::rhotn,Eh,Ex,eta,VCA,VCB,a,b
   complex(8),allocatable::gc(:)
 nrad = 10000
 allocate(urad(0:nrad),rrad(0:nrad),hurad(0:nrad),surad(0:nrad),g(0:nrad),sgrad(0:nrad),hhp(2,0:nrad),ssp(2,0:nrad))
 allocate(p(0:nrad),gc(0:nrad),w0(0:nrad),rho(0:nrad),rhot(0:nrad),w(0:nrad),vH(0:nrad),v1(0:nrad),v2(0:nrad),v1n(0:nrad),v2n(0:nrad),EC(0:nrad))
 pi = 4.d0*atan(1.d0)
 eta = 1.d0
 lang = 0
 znuc = 1.d0
 gama = 1.d-1
 !*************************************************
 call radgrid(nrad,rrad)
 do i = 0,nrad
 urad(i) = exp(-0.5*rrad(i)**2)
 enddo
!**************************************************  
do iter = 0,10

 call overlap(nrad,rrad,urad,sumn,surad)
 uradnrm = sqrt(sumn)
 do i = 0,nrad
 urad(i) = urad(i)/uradnrm
 enddo
 
 call overlap(nrad,rrad,urad,sumn,surad)
 call energr(nrad,lang,znuc,rrad,urad,ener,hurad)

 g = hurad - ener*surad
 call overlap(nrad,rrad,g,sumn,surad)
 gnrm = sqrt(sumn)
 write(*,'(i6,es24.15,es14.5)') iter,ener,gnrm
 if (gnrm < 1.d-3) exit

 call crtssp(nrad,rrad,ssp)
 call crthhp(nrad,lang,znuc,rrad,hhp)
 gc = cmplx(g,0.d0,8)
 call ctridag(nrad+1,hhp,ssp,ener,gama,gc)
 p = real(gc,8)

 do i=0,nrad
 urad(i) = urad(i) - p(i)
 enddo

enddo
!***************************************************end of preconditioned SD   
  do i = 0,nrad
    if (i==0) then
    w0(i) = (rrad(1)-rrad(0))/2
    elseif (i==nrad) then
        w0(i) = (rrad(nrad) - rrad(nrad-1))/2
    else
        w0(i) = (rrad(i+1) - rrad(i-1))/2
    endif
  enddo
!***************************************************
  do i = 0,nrad
  rho(i) =  1/(4*pi)*urad(i)**2  
  enddo
  
  do i = 0,nrad
  rhot(i) = 4*pi*rho(i)*rrad(i)**2*w0(i)
  enddo
  rhotn = sum(rhot(0:nrad))
!***************************************************
!  v1 =0.d0
!  v2 =0.d0
 
!  do irad = 0,nrad
!  w(0) = (rrad(1)-rrad(0))/2
!
!  do jrad = 1,irad-1
!  w(jrad) = (rrad(jrad+1)-rrad(jrad-1))/2
!  v1 = v1 + 4*pi*rho(jrad)*rrad(jrad)**2*w(jrad)
!  enddo
! 
!  w(irad) = (rrad(irad) - rrad(irad-1))/2
!
!  do j = irad,nrad
!  v2 = v2 + 4*pi*rho(i)*rrad(i)*w(i)
!  enddo

  v1(0) = 0.d0 
  v1(1) = 4.d0*pi*rrad(1)**2*rho(1)*(rrad(1) - rrad(0))/2
  do i = 2,nrad-1
  a =  4.d0*pi*rrad(i)**2*rho(i)*(rrad(i+1)-rrad(i-1))/2
  v1(i) = v1(i-1) + a
  enddo
  v1(nrad) =v1(nrad-1) + 4.d0*pi*rrad(nrad)**2*rho(nrad)*(rrad(nrad) - rrad(nrad-1))/2

!  v1(i) =  4*pi*rho(i)*rrad(i)**2*w0(i)
!  v1n(i) = sum(v1(0:i))

  v2(nrad) = 0.d0 
  v2(nrad-1) = 4.d0*pi*rrad(nrad-1)*rho(nrad-1)*(rrad(nrad) - rrad(nrad-1))/2
  do i = nrad-2,1,-1
   b =  4.d0*pi*rrad(i)*rho(i)*(rrad(i+1)-rrad(i-1))/2
   v2(i) = v2(i+1) + b
  enddo
  v2(0) = v2(1) + 4.d0*pi*rrad(nrad)*rho(nrad)*(rrad(1) - rrad(0))/2

!  v2(i) =  4*pi*rho(i)*rrad(i)*w0(i)
!  v2n(i) = sum(v2(0:i))
  
  do i = 1,nrad
  vH(i) = v1(i)/rrad(i) + v2(i) 
  enddo

  do i = 0,nrad
  call LSD_PADE(rho(i),eta,EC(i),VCA,VCB)
  enddo
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Eh =0.d0
Ex =0.d0
do i = 0,nrad

 Eh = Eh + 4*pi*1/2*vH(i)*rho(i)*w0(i)*rrad(i)**2
 Ex = Ex + 4.d0*pi*EC(i)*rho(i)*rrad(i)**2*w0(i)
!or Ex = Ex + EC(i)*urad(i)**2*rrad(i)**2*w0(i)
enddo

write(*,'(2es24.15,es14.5)') rhotn,Eh,Ex

end program hydrogenhex
