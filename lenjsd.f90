!!!!This program implement the steepest descent method for lennard jones potential
!!!author : Hossein Tahmasbi 18 october 2015 IASBS, Zanjan 
program lenjsd
implicit none

integer::nat,iat,i,iter
real(8)::epot,de,fmax,alpha,fn,epotold,ampl,rnd(3)
real(8),allocatable::fat(:,:)
real(8),allocatable::rat(:,:)

open(unit=1,file ='lj1000.dat',status='old')
read(1,*) nat
allocate(fat(3,nat),rat(3,nat))

!write(*,*) 'Coordinates of atoms:'
!write(*,*) '********************************************************************'
do iat=1,nat
read(1,*)  rat(1,iat),rat(2,iat),rat(3,iat)
!write(*,*) rat(1,iat),rat(2,iat),rat(3,iat)
enddo

ampl=0.1d0
do iat = 1,nat
call random_number(rnd)
rat(1:3,iat) = rat(1:3,iat) + (rnd(1:3) - 0.5d0)*1.d0*ampl
enddo

!write(*,*) 'Coordinates of random atoms:'
!write(*,*) '********************************************************************'
!do iat=1,nat
!write(*,*) rat(1,iat),rat(2,iat),rat(3,iat)
!enddo

! applying steepest descent
alpha = 1.d-3
epotold = 0.d0

do iter = 0,1000

    call lenjc(nat,rat,epot,fat)

    fn = sqrt(sum(fat(1:3,1:nat)**2))
    fmax = maxval(abs(fat))
    if(iter==0) epotold = epot
    de = epot - epotold
    write(*,'(i4,es24.15,3es14.5)') iter,epot,de,fn,fmax
    epotold = epot
    if(fmax < 1.d-3)  exit
    do iat = 1,nat
       do i = 1,3
          rat(i,iat)=rat(i,iat) + alpha*fat(i,iat)
       enddo
    enddo
enddo




end program lenjsd

!*********************************************************************************

subroutine lenjc(nat,rat,epot,fat)
implicit none

integer::iat,jat
integer,intent(in)::nat
real(8),intent(in)::rat(3,nat)
real(8),intent(out)::epot,fat(3,nat)
real(8)::dx,dy,dz,r2,r6,r8,r12,r14,vat
real(8)::tt,t1,t2,t3


epot = 0.d0
fat(1:3,1:nat) = 0.d0

do iat =1,nat


  do jat =iat+1,nat 


    dx=rat(1,iat)-rat(1,jat)
    dy=rat(2,iat)-rat(2,jat)
    dz=rat(3,iat)-rat(3,jat)
    r2=dx**2+dy**2+dz**2
    r6=r2**3
    r12=r2**6
    vat=4.d0*(1.d0/r12 - 1.d0/r6)

    epot =  epot + vat

    r8  = r2**4
    r14 = r2**7
    tt =24.d0*(2.d0/r14-1.d0/r8)
    t1 = tt*dx ; t2 = tt*dy ; t3 = tt*dz

    fat(1,iat) = fat(1,iat) + t1
    fat(2,iat) = fat(2,iat) + t2
    fat(3,iat) = fat(3,iat) + t3

    fat(1,jat) = fat(1,jat) - t1
    fat(2,jat) = fat(2,jat) - t2
    fat(3,jat) = fat(3,jat) - t3

  enddo

enddo

end subroutine lenjc
