!solving poisson's equation with the Conjugate Gradient method (CG)
!author : Hossein Tahmasbi,15 Nov 2015, IASBS
program cgpoisson
    implicit none
    integer::i,n,iter
    real(8)::pi,hg,alpha,gnrm,x,xi,gnrm2,gnrm2old,beta,hah,gmax
    real(8),allocatable::v(:),rho(:),g(:),vex(:),h(:),ah(:)
   
  n = 50000                                 !number of grid
  allocate(v(0:n),rho(n-1),g(n-1),vex(n-1),h(n-1),ah(n-1))
  
  pi = 4.d0*atan(1.d0)
  hg = 1.d0/n
     
  do i = 1,n-1
    x = pi *i*hg
    rho(i) = 10.d0*sin(10.d0*sin(x))*cos(x)**2 + sin(x)*cos(10.d0*sin(x))
    vex(i) = sin(10.d0*sin(x))/(10.d0*pi**2)+x/pi
  !  write(*,*) x/pi,rho(i),vex(i)
  enddo
     
  v(0) = 0.d0
  v(n) = 1.d0
  v(1:n-1) = 0.d0

!************************iteration********************************  
  do iter = 0,50000
    
  do i = 1,n-1 
      g(i) =-v(i+1) + 2*v(i) - v(i-1) - (hg**2)*rho(i)
  enddo 

  gmax = maxval(abs(g))  
  gnrm2 = sum(g(1:n-1)**2)
  gnrm = sqrt(gnrm2)
 
 ! write(*,'(i8,es14.5)') iter,gnrm
  if(gmax<1.d-7) exit
  if (iter==0) then
      h(1:n-1) = -g(1:n-1)
      else 
         beta = gnrm2 / gnrm2old
         h(1:n-1) = -g(1:n-1) + beta*h(1:n-1)
  endif
  
    ah(1) =-h(2) + 2*h(1)
  do i = 2,n-2
    ah(i) =-h(i+1) + 2*h(i) - h(i-1)
  enddo
    ah(n-1) = 2*h(n-1)- h(n-2)

  hah =  dot_product(h,ah)
  alpha = gnrm2/hah
  
  !do i = 1,n-1
  !   g(i) = g(i) + alpha*ah(i)
  !enddo


  do i = 1,n-1
  v(i) = v(i) + alpha * h(i)
  enddo

  gnrm2old = gnrm2
  
  enddo
!*****************************************************************  
  write(*,'(i8,es14.5)') iter,gnrm
!  do i =1,n-1
!  xi = i*hg
!  write(*,'(5es14.5)') xi,vex(i),v(i),rho(i),vex(i)-v(i)
!  enddo

end program cgpoisson
