!part1: This program implement the schrodinger equation for the hydrogen atom at ground state 
!author: Hossein Tahmasbi, Sunday 29 November 2015,IASBS,ZANJAN
program hydrogen
   implicit none
   integer::i,nrad,lang
   real(8),allocatable::rrad(:),urad(:),hurad(:),surad(:),g(:),sgrad(:)
   real(8)::znuc,ener,gnrm,uradnrm,sumn

 nrad = 1000
 allocate(urad(0:nrad),rrad(0:nrad),hurad(0:nrad),surad(0:nrad),g(0:nrad),sgrad(0:nrad))

 lang = 0
 znuc = 1.d0

 call radgrid(nrad,rrad)
 do i = 0,nrad
    urad(i) = 2*znuc**(1.5d0)*exp(-znuc*rrad(i))
 enddo

 call overlap(nrad,rrad,urad,sumn,surad)
 uradnrm = sqrt(sumn)
 do i = 0,nrad
 urad(i) = urad(i)/uradnrm
 enddo

 call energr(nrad,lang,znuc,rrad,urad,ener,hurad)

 
 call overlap(nrad,rrad,urad,sumn,surad)
 

 g = hurad - ener*surad

 call overlap(nrad,rrad,g,gnrm,sgrad)

write(*,*) ener,gnrm

end program hydrogen
