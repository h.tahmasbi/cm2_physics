!Part3(DFT): This program implement the schrodinger equation for the Helium atom and calculate the hartree and exchange-correlation energy with
!basis sets that are obtained by PC SD(program 1hydrogenpc.f90)
!input parameters :: DFT for Hydrogen: eta=1,nel=1,znuc=1 ***  DFT for Helium: eta=0,nel=2,znuc=2 *** DFT for Helium+: eta=1,nel=1,znuc=2
!author: Hossein Tahmasbi,Sunday 28 December 2015, IASBS, Zanjan, Presentation:Sunday 3 January 2016
program helium
   implicit none
   integer::i,nrad,lang,iter,j,nel
   real(8)::znuc,ener,gnrm,uradnrm,sumn,gama,pi
   real(8)::rhotn,Eh,Ex,eval,eta,ener_dft
   real(8),allocatable::rrad(:),urad(:),hurad(:),surad(:),g(:),hhp(:,:),hhphyd(:,:),ssp(:,:),da(:),daa(:)
   real(8),allocatable::p(:),w0(:),rho(:),rhot(:),vH(:),v1(:),v2(:),v1n(:),v2n(:),EC(:),VCA(:)
   complex(8),allocatable::gc(:)
 nrad = 10000
 allocate(urad(0:nrad),rrad(0:nrad),hurad(0:nrad),surad(0:nrad),hhp(2,0:nrad),ssp(2,0:nrad),hhphyd(2,0:nrad))
 allocate(g(0:nrad),VCA(0:nrad),da(0:nrad),daa(0:nrad))
 allocate(p(0:nrad),gc(0:nrad),w0(0:nrad),rho(0:nrad),rhot(0:nrad),vH(0:nrad),v1(0:nrad),v2(0:nrad),v1n(0:nrad),v2n(0:nrad),EC(0:nrad))

!****************input parameters*****************************************************
 lang = 0                !since we are only interested in s states ”lang” has to be set to 0
 eta  = 1.d0             !spin polarized 1.d0 , unpolarized 0.d0
 nel  = 1                !number of electrons
 znuc = 1.d0             !atomic number:Z 
 gama = 0.5d0            !parameter for preconditioning, usually 0.1 is a good value
 pi   = 4.d0*atan(1.d0)
!*************************************************************************************
 call radgrid(nrad,rrad)
 do i = 0,nrad
 urad(i) = exp(-0.5d0*rrad(i)**2)
 enddo
  
 call crtssp(nrad,rrad,ssp)
 call crthhp(nrad,lang,znuc,rrad,hhphyd)

! do i = 0,nrad
!   if (i==0) then
!       w0(i) = (rrad(1)-rrad(0))/2
!   elseif (i==nrad) then
!       w0(i) = (rrad(nrad) - rrad(nrad-1))/2
!   else
!       w0(i) = (rrad(i+1) - rrad(i-1))/2
!   endif
! enddo
!*************************************************************
do iter = 0,100

 call overlap(nrad,rrad,urad,sumn,surad)

 uradnrm = sqrt(sumn)
 do i = 0,nrad
 urad(i) = urad(i)/uradnrm
 enddo

 call harex_ener(znuc,nel,eta,nrad,rrad,urad,w0,vH,Eh,Ex,VCA)

 do i = 0,nrad
 da(i) = (vH(i) + VCA(i))*(rrad(i)**2)*urad(i)*w0(i) 
 daa(i) = (vH(i) + VCA(i))*(rrad(i)**2)*w0(i) 
 enddo

 call overlap(nrad,rrad,urad,sumn,surad)
 call energr(nrad,lang,znuc,rrad,urad,ener,hurad)

 ener_dft = nel*ener + Ex + Eh
 eval = ener + dot_product(da,urad)
 do i = 0,nrad
 g(i) = nel*hurad(i) + da(i) - eval*surad(i)
 enddo

 call overlap(nrad,rrad,g,sumn,surad)
 gnrm = sqrt(sumn)
 write(*,'(i6,es24.15,es14.5)') iter,ener_dft,gnrm
 if (gnrm < 1.d-13) exit
 hhp(1,:) = nel*hhphyd(1,:) + daa(0:nrad)
 hhp(2,:) = nel*hhphyd(2,:)
 gc = cmplx(g,0.d0,8)         !gc is either an input and output parameter
 call ctridag(nrad+1,hhp,ssp,eval,gama,gc)
 p = real(gc,8)

 do i=0,nrad
 urad(i) = urad(i) - p(i)
 enddo

enddo
!*********************************************************end of preconditioning  
!ener_dft = nel*ener + Ex + Eh
write(*,'(5es24.15)') Eh,Ex,eval,ener,ener_dft
write(*,*) 'Hartree Energy              =',Eh
write(*,*) 'Exchange-Correlation Energy =',Ex
write(*,*) 'Total Energy                =',ener_dft

end program helium

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!author: Hossein Tahmasbi,Tuesday 8 Deceber 2015, IASBS, Zanjan
subroutine harex_ener(znuc,nel,eta,nrad,rrad,urad,w0,vH,Eh,Ex,VCA)
   implicit none
   integer::i
   integer,intent(in)::nrad,nel
   real(8),intent(in)::znuc,eta
   real(8),intent(in)::rrad(0:nrad),urad(0:nrad)
   real(8),intent(out)::Eh,Ex
   real(8),intent(out)::w0(0:nrad),vH(0:nrad),VCA(0:nrad)
   real(8)::rhotn,ener,gama,pi
   real(8)::rho(0:nrad),rhot(0:nrad),VCB(0:nrad)
   real(8)::v1(0:nrad),v2(0:nrad),v1n(0:nrad),v2n(0:nrad),EC(0:nrad)
 pi = 4.d0*atan(1.d0)
!*********************************************************
  do i = 0,nrad
    if (i==0) then
        w0(i) = (rrad(1)-rrad(0))/2
    elseif (i==nrad) then
        w0(i) = (rrad(nrad) - rrad(nrad-1))/2
    else
        w0(i) = (rrad(i+1) - rrad(i-1))/2
    endif
  enddo

  do i = 0,nrad
  rho(i) = nel* 1/(4.d0*pi)*urad(i)**2  
  enddo
  
  do i = 0,nrad
  rhot(i) = 4.d0*pi*rho(i)*rrad(i)**2*w0(i)
  enddo
  rhotn = sum(rhot(0:nrad))

 do i = 0,nrad

  v1(i) =  4.d0*pi*rho(i)*rrad(i)**2*w0(i)
  v1n(i) = sum(v1(0:i))

  v2(i) =  4.d0*pi*rho(i)*rrad(i)*w0(i)
  v2n(i) = sum(v2(0:i))

 enddo
  
 do i = 1,nrad
  vH(i) = v1n(i)/rrad(i) + (v2n(nrad)-v2n(i))
 enddo

 do i = 0,nrad
 
 if (rho(i) > 1.d-20) then
     call LSD_PADE(rho(i),eta,EC(i),VCA(i),VCB(i))
 else
     EC(i)=0.d0;  VCA(i) =0.d0; VCB(i)=0.d0; 
 endif

 enddo
Eh =0.d0
Ex =0.d0

  do i = 0,nrad
  Eh = Eh + 4.d0*pi*1/2*vH(i)*rho(i)*w0(i)*rrad(i)**2
  Ex = Ex + 4.d0*pi*EC(i)*rho(i)*w0(i)*rrad(i)**2
!or Ex = Ex + EC(i)*urad(i)**2*rrad(i)**2*w0(i)
  enddo 
!write(*,'(2es24.15,es14.5)') rhotn,Eh,Ex

end subroutine harex_ener
