!Part2_1: This program implement the schrodinger equation for the hydrogen atom and calculate the hartree and the exchange correllation energy with
!basis sets that are obtained by PC SD(program 1hydrogenpc.f90) 
!author: Hossein Tahmasbi,Tuesday 8 Deceber 2015, IASBS, Zanjan
program hydrogenharex
   implicit none
   integer::i,nrad,lang,iter,j,irad,jrad
   real(8),allocatable::rrad(:),urad(:),hurad(:),surad(:),g(:),sgrad(:),hhp(:,:),ssp(:,:)
   real(8)::znuc,ener,gnrm,uradnrm,sumn,gama,pi
   real(8),allocatable::p(:),w0(:),rho(:),rhot(:),w(:),vH(:),v1(:),v2(:),v1n(:),v2n(:),EC(:)
   real(8)::rhotn,Eh,Ex,eta,ener_dft,VCA,VCB
   complex(8),allocatable::gc(:)
 nrad = 10000
 allocate(urad(0:nrad),rrad(0:nrad),hurad(0:nrad),surad(0:nrad),g(0:nrad),sgrad(0:nrad),hhp(2,0:nrad),ssp(2,0:nrad))
 allocate(p(0:nrad),gc(0:nrad),w0(0:nrad),rho(0:nrad),rhot(0:nrad),w(0:nrad),vH(0:nrad),v1(0:nrad),v2(0:nrad),v1n(0:nrad),v2n(0:nrad),EC(0:nrad))
 pi = 4.d0*atan(1.d0)
!*************************************************
 eta = 1.d0             !for spin polarized atom should be 1.d0 and otherwise 0.d0
 lang = 0               !we are only interested in "s" states therefore ”lang” has to be set to 0
 znuc = 1.d0
 gama = 1.d-1
!*************************************************
 call radgrid(nrad,rrad)
 do i = 0,nrad
 urad(i) = exp(-0.5*rrad(i)**2)     !an input guess for the wave-function
 enddo


 call crtssp(nrad,rrad,ssp)
 call crthhp(nrad,lang,znuc,rrad,hhp)

!*************************************************  
do iter = 0,10

 call overlap(nrad,rrad,urad,sumn,surad)
 uradnrm = sqrt(sumn)
 do i = 0,nrad
 urad(i) = urad(i)/uradnrm          !normalization of the wave-function
 enddo
 
 call overlap(nrad,rrad,urad,sumn,surad)
 call energr(nrad,lang,znuc,rrad,urad,ener,hurad)

 g = hurad - ener*surad
 call overlap(nrad,rrad,g,sumn,surad)
 gnrm = sqrt(sumn)
 write(*,'(i6,es24.15,es14.5)') iter,ener,gnrm
 if (gnrm < 1.d-3) exit

 gc = cmplx(g,0.d0,8)
 call ctridag(nrad+1,hhp,ssp,ener,gama,gc)
 p = real(gc,8)

 do i=0,nrad
 urad(i) = urad(i) - p(i)
 enddo

enddo
!****************************************************************end of preconditioning  
  do i = 0,nrad
    if (i==0) then
    w0(i) = (rrad(1)-rrad(0))/2
    elseif (i==nrad) then
        w0(i) = (rrad(nrad) - rrad(nrad-1))/2
    else
        w0(i) = (rrad(i+1) - rrad(i-1))/2
    endif
  enddo

  do i = 0,nrad
  rho(i) =  1/(4.d0*pi)*urad(i)**2  
  enddo
  
  do i = 0,nrad
  rhot(i) = 4.d0*pi*rho(i)*rrad(i)**2*w0(i)
  enddo
  rhotn = sum(rhot(0:nrad))


 do i = 0,nrad


  v1(i) =  4*pi*rho(i)*rrad(i)**2*w0(i)
  v1n(i) = sum(v1(0:i))

  v2(i) =  4*pi*rho(i)*rrad(i)*w0(i)
  v2n(i) = sum(v2(0:i))

 enddo
  
 do i = 1,nrad
  vH(i) = v1n(i)/rrad(i) + (v2n(nrad)-v2n(i))
 enddo

 do i = 0,nrad
  call LSD_PADE(rho(i),eta,EC(i),VCA,VCB)
 enddo

Eh =0.d0
Ex =0.d0

do i = 0,nrad
  Eh = Eh + 4*pi*1/2*vH(i)*rho(i)*w0(i)*rrad(i)**2       !the electrostatic energy
  Ex = Ex + 4.d0*pi*EC(i)*rho(i)*rrad(i)**2*w0(i)        !the Exchange-Correlation  energy 
!or Ex = Ex + EC(i)*urad(i)**2*rrad(i)**2*w0(i)
enddo                                                    !If LDA density functional theory was exact, the sum of both(Eh+Ex)would be zero.
ener_dft = ener + Eh + Ex 
write(*,'(2es24.15,4es14.5)') rhotn,ener,Eh,Ex,Eh+Ex,ener_dft

end program hydrogenharex
