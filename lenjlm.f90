!!!!This program implement the steepest descent iteration with line minimization for lennard-jones potential
!!!author : Hossein Tahmasbi 27 october 2015 IASBS, Zanjan 
program lenjlm
implicit none

integer::nat,iat,i,iter
real(8)::ampl,epot,epot_t,epotold,de,fmax,fn,rnd(3)
real(8)::alphax,alphat,alphamin,phiprimt,phiprim0,a,b
real(8),allocatable::fat(:,:),fat_t(:,:)
real(8),allocatable::rat(:,:),rat_t(:,:)

open(unit=1,file ='lj1000.dat',status='old')
read(1,*) nat
allocate(fat(3,nat),rat(3,nat),fat_t(3,nat),rat_t(3,nat))

!write(*,*) 'Coordinates of atoms:'
!write(*,*) '********************************************************************'
do iat=1,nat
read(1,*)  rat(1,iat),rat(2,iat),rat(3,iat)
!write(*,*) rat(1,iat),rat(2,iat),rat(3,iat)
enddo

ampl=0.1d0
do iat = 1,nat
call random_number(rnd)
rat(1:3,iat) = rat(1:3,iat) + (rnd(1:3) - 0.5d0)*1.d0*ampl
enddo

!write(*,*) 'Coordinates of random atoms:'
!write(*,*) '********************************************************************'
!do iat=1,nat
!write(*,*) rat(1,iat),rat(2,iat),rat(3,iat)
!enddo

alphax = 1.d-3
alphat = 2*alphax    !alpha trial
alphamin = 0.d0

do iter = 0,1000
    call lenjc(nat,rat,epot,fat)
      
    fn = sqrt(sum(fat(1:3,1:nat)**2))
    fmax = maxval(abs(fat))
    if (iter == 0) epotold = epot
    de = epot - epotold    
    write(*,'(i4,es24.15,4es14.5)') iter,epot,de,fn,fmax,alphamin/alphax
    
    if(fmax < 1.d-3)  exit
    
    epotold = epot      
    do iat = 1,nat
       do i = 1,3
          rat_t(i,iat)=rat(i,iat) + alphat*fat(i,iat)
       enddo
    enddo

    call lenjc(nat,rat_t,epot_t,fat_t)

    phiprimt = sum(fat(1:3,1:nat)*fat_t(1:3,1:nat))
    phiprim0 = sum(fat(1:3,1:nat)*fat(1:3,1:nat))
    a = (phiprimt - phiprim0)/(2*alphat)
    b = phiprim0 
    alphamin = -b / (2*a)

    do iat = 1,nat
       do i = 1,3
          rat(i,iat)=rat(i,iat) + alphamin*fat(i,iat)
       enddo
    enddo

enddo




end program lenjlm

!*********************************************************************************

subroutine lenjc(nat,rat,epot,fat)
implicit none

integer::iat,jat
integer,intent(in)::nat
real(8),intent(in)::rat(3,nat)
real(8),intent(out)::epot,fat(3,nat)
real(8)::dx,dy,dz,r2,r6,r8,r12,r14,vat
real(8)::tt,t1,t2,t3


epot = 0.d0
fat(1:3,1:nat) = 0.d0

do iat =1,nat


  do jat =iat+1,nat 


    dx=rat(1,iat)-rat(1,jat)
    dy=rat(2,iat)-rat(2,jat)
    dz=rat(3,iat)-rat(3,jat)
    r2=dx**2+dy**2+dz**2
    r6=r2**3
    r12=r2**6
    vat=4.d0*(1.d0/r12 - 1.d0/r6)

    epot =  epot + vat

    r8  = r2**4
    r14 = r2**7
    tt =24.d0*(2.d0/r14-1.d0/r8)
    t1 = tt*dx ; t2 = tt*dy ; t3 = tt*dz

    fat(1,iat) = fat(1,iat) + t1
    fat(2,iat) = fat(2,iat) + t2
    fat(3,iat) = fat(3,iat) + t3

    fat(1,jat) = fat(1,jat) - t1
    fat(2,jat) = fat(2,jat) - t2
    fat(3,jat) = fat(3,jat) - t3

  enddo

enddo

end subroutine lenjc
