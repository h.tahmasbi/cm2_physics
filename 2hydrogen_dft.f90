!Part2_2: This program calculate the hartree and exchange energy for the Hydrogen atom with the exact wave function 
!calculating the DFT energy shown that DFT do not work good.***ener_dft is equal to -0.477777***  
!author: Hossein Tahmasbi,Tuesday 27 Deceber 2015, IASBS, Zanjan
program hydrogenhex_dft
   implicit none
   integer::i,nrad,lang,iter,j
   real(8),allocatable::rrad(:),urad(:),hurad(:),surad(:),g(:),sgrad(:),hhp(:,:),ssp(:,:)
   real(8)::znuc,ener,gnrm,uradnrm,sumn,gama,pi
   real(8),allocatable::p(:),w0(:),rho(:),rhot(:),w(:),vH(:),v1(:),v2(:),v1n(:),v2n(:),EC(:)
   real(8)::rhotn,Eh,Ex,eta,VCA,VCB,ener_dft
   complex(8),allocatable::gc(:)
 nrad = 10000
 allocate(urad(0:nrad),rrad(0:nrad),hurad(0:nrad),surad(0:nrad),g(0:nrad),sgrad(0:nrad),hhp(2,0:nrad),ssp(2,0:nrad))
 allocate(p(0:nrad),gc(0:nrad),w0(0:nrad),rho(0:nrad),rhot(0:nrad),w(0:nrad),vH(0:nrad),v1(0:nrad),v2(0:nrad),v1n(0:nrad),v2n(0:nrad),EC(0:nrad))
 pi = 4.d0*atan(1.d0)
 eta = 1.d0
 lang = 0
 znuc = 1.d0
 gama = 1.d-1
 call radgrid(nrad,rrad)
 do i = 0,nrad
 urad(i) = 2.d0* exp(-rrad(i))
 enddo
  
 call energr(nrad,lang,znuc,rrad,urad,ener,hurad)
!******************************************************** 
  do i = 0,nrad
    if (i==0) then
    w0(i) = (rrad(1)-rrad(0))/2
    elseif (i==nrad) then
        w0(i) = (rrad(nrad) - rrad(nrad-1))/2
    else
        w0(i) = (rrad(i+1) - rrad(i-1))/2
    endif
  enddo
!********************************************************
  do i = 0,nrad
  rho(i) =  1/(4*pi)*urad(i)**2  
  enddo
  
  do i = 0,nrad
  rhot(i) = 4*pi*rho(i)*rrad(i)**2*w0(i)
  enddo
  rhotn = sum(rhot(0:nrad))
!********************************************************
do i = 0,nrad

  v1(i) =  4*pi*rho(i)*rrad(i)**2*w0(i)
  v1n(i) = sum(v1(0:i))

  v2(i) =  4*pi*rho(i)*rrad(i)*w0(i)
  v2n(i) = sum(v2(0:i))

enddo
!********************************************************  
do i = 1,nrad
  vH(i) = v1n(i)/rrad(i) + (v2n(nrad)-v2n(i))
enddo

do i = 0,nrad
  call LSD_PADE(rho(i),eta,EC(i),VCA,VCB)
enddo

Eh =0.d0
Ex =0.d0

do i = 0,nrad
  Eh = Eh + 4*pi*1/2*vH(i)*rho(i)*w0(i)*rrad(i)**2
  Ex = Ex + 4.d0*pi*EC(i)*rho(i)*rrad(i)**2*w0(i)
!or Ex = Ex + EC(i)*urad(i)**2*rrad(i)**2*w0(i)
enddo

ener_dft = ener + Eh + Ex 
write(*,'(3es24.15,2es14.5)') rhotn,ener,Eh,Ex,ener_dft

end program hydrogenhex_dft
