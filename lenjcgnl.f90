!This program implement nonlinear conjugate gradient (CG) for lennard-jones potential
!author: Hossein Tahmasbi, 24 november 2015,IASBS,Zanjan

program lenjcgnl
    implicit none
    integer::i,iat,nat,iter 
    real(8),allocatable::fat(:,:),rat(:,:),h(:,:),df(:,:),fat2(:,:)
    real(8)::ampl,gg,dff,beta,fmax,alphamin,epot,epot2,rnd(3),de
    

open(unit=1,file ='lj1000.dat',status='old')
read(1,*) nat
allocate(fat(3,nat),rat(3,nat),fat2(3,nat),h(3,nat),df(3,nat))

do iat = 1,nat
read(1,*)  rat(1,iat),rat(2,iat),rat(3,iat)
enddo

ampl=0.1d0
do iat = 1,nat
call random_number(rnd)
rat(1:3,iat) = rat(1:3,iat) + (rnd(1:3) - 0.5d0)*1.d0*ampl
enddo

call lenjc(nat,rat,epot,fat)

!************************************************************************nonlinear CG
do iter = 0,500

call lm(nat,rat,epot,fat,alphamin)

if(iter==0) then
    h = fat
endif

rat(1:3,1:nat) = rat(1:3,1:nat) + alphamin*h(1:3,1:nat)


call lenjc(nat,rat,epot2,fat2)
de = epot2 - epot
fmax = maxval(abs(fat2))
write(*,'(i4,4es18.9)') iter,de,epot2,fmax,alphamin
if(fmax<1.d-3) exit

df = fat2 - fat

dff = 0.d0
do iat = 1,nat
do i = 1,3
dff = dff + df(i,iat)*fat2(i,iat)
enddo
enddo

gg = sum(fat(1:3,1:nat)**2)
beta = dff/gg

h = fat2 + beta * h

fat = fat2
epot = epot2

enddo

end program lenjcgnl

!**********************************************************************************line
subroutine lm(nat,rat,epot,fat,alphamin)
   implicit none

integer::nat,iat,i,iter
real(8)::epot,epot_t
real(8)::alphax,alphat,alphamin,phiprimt,phiprim0,a,b
real(8)::rat(3,nat),fat(3,nat)
real(8)::rat_t(3,nat),fat_t(3,nat)



alphax = 1.d-3
alphat = 2*alphax    !alpha trial

!    do iat = 1,nat
!       do i = 1,3
!          rat_t(i,iat)=rat(i,iat) + alphat*fat(i,iat)
!       enddo
!    enddo
    rat_t(1:3,1:nat)=rat(1:3,1:nat) + alphat*fat(1:3,1:nat)

    call lenjc(nat,rat_t,epot_t,fat_t)

    phiprimt = sum(fat(1:3,1:nat)*fat_t(1:3,1:nat))
    phiprim0 = sum(fat(1:3,1:nat)*fat(1:3,1:nat))
    a = (phiprimt - phiprim0)/(2*alphat)
    b = phiprim0 
    alphamin = -b / (2*a)

end subroutine lm

!*********************************************************************************************lenj
  subroutine lenjc(nat,rat,epot,fat)
   implicit none

   integer::iat,jat
   integer,intent(in)::nat
   real(8),intent(in)::rat(3,nat)
   real(8),intent(out)::epot,fat(3,nat)
   real(8)::dx,dy,dz,r2,r6,r8,r14,r12,tt,vat


   epot = 0.d0
   fat(1:3,1:nat) = 0.d0

   do iat =1,nat


   do jat =iat+1,nat 


   dx=rat(1,iat)-rat(1,jat)
   dy=rat(2,iat)-rat(2,jat)
   dz=rat(3,iat)-rat(3,jat)
   r2=dx**2+dy**2+dz**2
   r6=r2**3
   r12=r2**6
   vat=4.d0*(1.d0/r12 - 1.d0/r6)

   epot =  epot + vat

   r8 = r2**4
   r14 = r2**7
   tt =24.d0*(2.d0/r14 - 1.d0/r8)

   fat(1,iat) = fat(1,iat) + tt*dx
   fat(2,iat) = fat(2,iat) + tt*dy
   fat(3,iat) = fat(3,iat) + tt*dz

   fat(1,jat) = fat(1,jat) - tt*dx
   fat(2,jat) = fat(2,jat) - tt*dy
   fat(3,jat) = fat(3,jat) - tt*dz

   enddo
   enddo

  end subroutine lenjc
!*********************************************************************
