!part1: This program implement the schrodinger equation for the hydrogen atom at ground state with the preconditioned steepest
!descent method 
!author: Hossein Tahmasbi,Sunday 6 December 2015,IASBS,ZANJAN
program hydrogenpc
   implicit none
   integer::i,nrad,lang,iter
   real(8),allocatable::rrad(:),urad(:),hurad(:),surad(:),g(:),sgrad(:),hhp(:,:),ssp(:,:)
   real(8)::znuc,ener,gnrm,uradnrm,sumn,gama
   real(8),allocatable::p(:)
   complex(8),allocatable::gc(:)
 nrad = 100
 allocate(urad(0:nrad),rrad(0:nrad),hurad(0:nrad),surad(0:nrad),g(0:nrad),sgrad(0:nrad),hhp(2,0:nrad),ssp(2,0:nrad),p(0:nrad),gc(0:nrad))

 lang = 0
 znuc = 1.d0
 gama = 1.d-1
 call radgrid(nrad,rrad)
 do i = 0,nrad
 urad(i) = exp(-0.5*rrad(i)**2)
 enddo
  
do iter = 0,10

 call overlap(nrad,rrad,urad,sumn,surad)
 uradnrm = sqrt(sumn)
 do i = 0,nrad
 urad(i) = urad(i)/uradnrm
 enddo
 
 call overlap(nrad,rrad,urad,sumn,surad)
 call energr(nrad,lang,znuc,rrad,urad,ener,hurad)

 g = hurad - ener*surad
 call overlap(nrad,rrad,g,sumn,surad)
 gnrm = sqrt(sumn)
 write(*,'(i6,es24.15,es14.5)') iter,ener,gnrm
 if (gnrm < 1.d-3) exit

 call crtssp(nrad,rrad,ssp)
 call crthhp(nrad,lang,znuc,rrad,hhp)
 gc = cmplx(g,0.d0,8)
 call ctridag(nrad+1,hhp,ssp,ener,gama,gc)
 p = real(gc,8)

 do i=0,nrad
 urad(i) = urad(i) - p(i)
 enddo

enddo
end program hydrogenpc
