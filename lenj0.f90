!!!!This program is written to calculate lennard-jones potential for a structure
!!!author : Hossein Tahmasbi 11 october 2015, IASBS, Zanjan
program lenj0
implicit none

integer::nat,iat ! ,m,i,l
real(8)::epot,fatx,faty,fatz,rnd(3),ampl !,h,fd,rb,c(-2:2)
real(8),allocatable::fat(:,:) ! , fatref(:,:)
real(8),allocatable::rat(:,:)

open(unit=1,file ='lj1000.dat',status='old')
read(1,*) nat
allocate(fat(3,nat),rat(3,nat))
!allocate(fatref(3,nat))

!write(*,*) 'Coordinates of atoms:'
!write(*,*) '********************************************************************'
do iat=1,nat
read(1,*)  rat(1,iat),rat(2,iat),rat(3,iat)
!write(*,*) rat(1,iat),rat(2,iat),rat(3,iat)
enddo

!if the coordinates of atoms were random , potential of min point will increase
ampl=0.1d0
do iat = 1,nat
call random_number(rnd)
rat(1:3,iat) = rat(1:3,iat) + (rnd(1:3) - 0.5d0)*2.d0*ampl
enddo



!write(*,*) 'Coordinates of random atoms:'
!write(*,*) '********************************************************************'
!do iat=1,nat
!write(*,*) rat(1,iat),rat(2,iat),rat(3,iat)
!enddo


call lenjc(nat,rat,epot,fat) 


write(*,*) '*********************************************************************'
write(*,*) 'Potential Energy:'

write(*,*) epot

write(*,*) '*********************************************************************'
write(*,*) 'Force on each atom:'


fatx = 0.d0
faty = 0.d0
fatz = 0.d0

do iat = 1,nat
!write(*,*)  iat
write(*,'(3es24.15)')  fat(1,iat),fat(2,iat),fat(3,iat)
fatx = fatx + fat(1,iat)
faty = faty + fat(2,iat)
fatz = fatz + fat(3,iat)
enddo

write(*,*) '*********************************************************************'
write(*,*) 'total force :'
write(*,'(3es24.15)') fatx,faty,fatz


end program lenj0


!**************************************************************************************************************

subroutine lenjc(nat,rat,epot,fat)
implicit none

integer::iat,jat
integer,intent(in)::nat
real(8),intent(in)::rat(3,nat)
real(8),intent(out)::epot,fat(3,nat)
real(8)::dx,dy,dz,r2,r6,r8,r14,r12,tt,vat


epot = 0.d0
fat(1:3,1:nat) = 0.d0

do iat =1,nat


do jat =iat+1,nat 


    dx=rat(1,iat)-rat(1,jat)
    dy=rat(2,iat)-rat(2,jat)
    dz=rat(3,iat)-rat(3,jat)
    r2=dx**2+dy**2+dz**2
    r6=r2**3
    r12=r2**6
    vat=4.d0*(1.d0/r12 - 1.d0/r6)

    epot =  epot + vat

    r8 = r2**4
    r14 = r2**7
    tt =24.d0*(2.d0/r14 - 1.d0/r8)

    fat(1,iat) = fat(1,iat) + tt*dx
    fat(2,iat) = fat(2,iat) + tt*dy
    fat(3,iat) = fat(3,iat) + tt*dz

    fat(1,jat) = fat(1,jat) - tt*dx
    fat(2,jat) = fat(2,jat) - tt*dy
    fat(3,jat) = fat(3,jat) - tt*dz

enddo
enddo

end subroutine lenjc
