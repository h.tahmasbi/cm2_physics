!part1: This program implement the schrodinger equation for the hydrogen atom at ground state with the steepest descent
!author: Hossein Tahmasbi,Sunday 6 December 2015,IASBS,ZANJAN
program hydrogensd
   implicit none
   integer::i,iter,nrad,lang
   real(8),allocatable::rrad(:),urad(:),hurad(:),surad(:),g(:),sgrad(:)
   real(8)::znuc,ener,gnrm,uradnrm,sumn,alpha

 nrad = 100
 allocate(urad(0:nrad),rrad(0:nrad),hurad(0:nrad),surad(0:nrad),g(0:nrad),sgrad(0:nrad))

 alpha = 2*1.d-5
 lang = 0
 znuc = 1.d0

 call radgrid(nrad,rrad)
 do i = 0,nrad
    urad(i) = exp(-rrad(i)**2)
 enddo

do iter = 0,5000000

 call overlap(nrad,rrad,urad,sumn,surad)
 uradnrm = sqrt(sumn)
 do i = 0,nrad
 urad(i) = urad(i)/uradnrm
 enddo

 call energr(nrad,lang,znuc,rrad,urad,ener,hurad)
 
 g = hurad - ener*surad
 call overlap(nrad,rrad,g,sumn,surad)
 gnrm = sqrt(sumn)

! if (mod(iter,1000)==0) then
! write(*,*) ener,iter 
! endif
 if (gnrm <1.d-3) exit

 urad = urad - alpha*g
 write(*,*) iter,ener,gnrm 

enddo

! write(*,*) ener,iter 

end program hydrogensd
