!solving poisson's equation with SD method
!author : Hossein Tahmasbi,10 Nov 2015, IASBS
program sdpoisson
    implicit none
    integer::i,n,iter
    real(8)::pi,h,alpha,gnrm,x,xi
    real(8),allocatable::v(:),rho(:),g(:),vex(:)
   
  n = 100                                 !number of grid
  allocate(v(0:n),rho(n-1),g(n-1),vex(n-1))
  
  alpha =25.d-2
  pi = 4.d0*atan(1.d0)
  h = 1.d0/n
     
  do i = 1,n-1
    x = pi *i*h
    rho(i) = 10.d0*sin(10.d0*sin(x))*cos(x)**2 + sin(x)*cos(10.d0*sin(x))
    vex(i) = sin(10.d0*sin(x))/(10.d0*pi**2)+x/pi
  !  write(*,*) x/pi,rho(i),vex(i)
  enddo
  
   
  v(0) = 0.d0
  v(n) = 1.d0

  v(1:n-1) = 0.d0

  do iter = 1,1000000

  do i = 1,n-1
     g(i) =-v(i+1) + 2*v(i) - v(i-1) - (h**2)*rho(i)
  enddo  

  gnrm = sqrt(sum(g(1:n-1)**2))
 ! write(*,'(i8,es14.5)') iter,gnrm
  if(gnrm<1.d-3) exit

     
  do i = 1,n-1
  v(i) = v(i) - alpha * g(i)
  enddo 
 
  enddo

 ! write(*,'(i8,es14.5)') iter,gnrm
  do i =1,n-1
  xi = i*h
  write(*,'(5es14.5)') xi,vex(i),v(i),rho(i),vex(i)-v(i)
  enddo

end program sdpoisson
